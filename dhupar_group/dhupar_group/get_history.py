import frappe
import json
import logging

# logging.basicConfig(filename='debugging_aj.log', level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')

@frappe.whitelist()
def get_item_history(customer, items, items_array, invoice):
    # logging.info(customer)
    # logging.info(items)
    # logging.info(items_array)
    # logging.info(invoice)
    items = json.loads(items)
    items = [str(i['item_code']) for i in items if i['name'] in items_array]

    invoices =  frappe.db.sql("select name from `tabSales Invoice` where customer_name = '%s'" % str(customer))
    # logging.info(invoice)
    k = [ str(i[0]) for i in invoices]
    if invoice in k :
        k.remove(invoice)
    # logging.info(k)
    format_invoices = ','.join(['\'%s\''] * len(k))
    result = []

    for item in items:
        result1 = frappe.db.sql("""select creation, item_code, item_name, qty, price_list_rate, discount_percentage 
                                    from `tabSales Invoice Item` 
                                    where parent in ({invoices}) and item_code = ('{items}')
                                    ORDER BY creation desc limit 5""".format(invoices = format_invoices % tuple(k), items = item))
        
        result.append(result1)

    return result
